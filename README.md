# README #

Code for investigating a possible bug in Semaphore.

Includes an OrderedExecutor interface adapted from Hanson Char's specification, two implementations, and unit tests.

testOrderedExecutorConditionQueue passes, but testOrderedExecutorSemaphore times out.
