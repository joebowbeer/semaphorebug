package com.joebowbeer.semaphorebug;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import static org.junit.Assert.*;

public class OrderedExecutorTest {
  
  private static final int NUM_ORDERS = 100;

  @Test
  public void testOrderedExecutorConditionQueue() throws InterruptedException {
    System.out.println("testOrderedExecutorConditionQueue");
    doExecCallableInOrder(new OrderedExecutorConditionQueue<>());
  }

  @Test(timeout = 2000)
  public void testOrderedExecutorSemaphore() throws InterruptedException {
    System.out.println("testOrderedExecutorSemaphore");
    doExecCallableInOrder(new OrderedExecutorSemaphore<>());
  }

  private void doExecCallableInOrder(OrderedExecutor<Integer> exec) throws InterruptedException {
    final List<Integer> result = new ArrayList<>();

    List<Runnable> runnables = new ArrayList<>();
    for (int i = 0; i < NUM_ORDERS; i++) {
      final int order = i;
      runnables.add((Runnable) () -> {
        System.out.println("Waiting " + order);
        try {
          exec.execCallableInOrder(order, () -> {
            System.out.println("Executing " + order);
            result.add(order);
            return order;
          });
        } catch (Exception ex) {
          throw new IllegalStateException(ex);
        }
      });
    }
    Collections.shuffle(runnables);

    ExecutorService service = Executors.newCachedThreadPool();
    for (Runnable runnable : runnables) {
      service.execute(runnable);
    }
    service.shutdown();
    service.awaitTermination(1, TimeUnit.DAYS);

    assertEquals(NUM_ORDERS, result.size());
    for (int i = 0; i < NUM_ORDERS; i++) {
      assertEquals(i, (int) result.get(i));
    }
  }
}
