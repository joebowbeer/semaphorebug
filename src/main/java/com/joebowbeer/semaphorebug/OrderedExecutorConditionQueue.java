package com.joebowbeer.semaphorebug;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class OrderedExecutorConditionQueue<T> implements OrderedExecutor<T> {

  private static class Waiter implements Comparable<Waiter> {

    final int order;
    final Condition condition;

    Waiter(int order, Condition condition) {
      this.order = order;
      this.condition = condition;
    }

    @Override
    public int compareTo(Waiter waiter) {
      return order - waiter.order;
    }
  }

  private final Lock lock = new ReentrantLock();
  private final Queue<Waiter> queue = new PriorityQueue<>();
  private int nextOrder;

  @Override
  public T execCallableInOrder(int order, Callable<T> callable)
      throws InterruptedException, Exception {
    assert order >= 0;
    awaitTurn(order);
    try {
      return callable.call();
    } finally {
      signalNext(order + 1);
    }
  }

  private void awaitTurn(int order) throws InterruptedException {
    lock.lockInterruptibly();
    try {
      Condition condition = null;
      while (nextOrder != order) {
        if (condition == null) {
          condition = lock.newCondition();
          queue.add(new Waiter(order, condition));
        }
        condition.await();
      }
    } finally {
      lock.unlock();
    }
  }

  private void signalNext(int nextOrder) {
    lock.lock();
    try {
      this.nextOrder = nextOrder;
      Waiter waiter = queue.peek();
      if (waiter != null && waiter.order == nextOrder) {
        queue.remove();
        waiter.condition.signal();
      }
    } finally {
      lock.unlock();
    }
  }
}
