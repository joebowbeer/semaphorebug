package com.joebowbeer.semaphorebug;

import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;

public class OrderedExecutorSemaphore<T> implements OrderedExecutor<T> {
  
  private final Semaphore semaphore = new Semaphore(1);

  @Override
  public T execCallableInOrder(int order, Callable<T> callable)
      throws InterruptedException, Exception {
    assert order >= 0;
    int acquires = order + 1;
    semaphore.acquire(acquires);
    try {
      return callable.call();
    } finally {
      semaphore.release(acquires + 1);
    }
  }
}
