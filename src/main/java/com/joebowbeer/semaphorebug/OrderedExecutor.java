package com.joebowbeer.semaphorebug;

import java.util.concurrent.Callable;

public interface OrderedExecutor<T> {

  T execCallableInOrder(int order, Callable<T> callable) throws InterruptedException, Exception;
}
